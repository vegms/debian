#!/bin/bash
echo
echo "Aktualisierung"
date
echo
#echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
apt-get update
apt-get --fix-broken install
apt-get --yes upgrade
apt-get --yes autoremove
apt-get --yes clean
apt-get --yes autoclean
#echo 0 > /proc/sys/net/ipv6/conf/all/disable_ipv6
exit 0